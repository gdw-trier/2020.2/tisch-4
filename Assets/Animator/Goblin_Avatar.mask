%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Goblin_Avatar
  m_Mask: 01000000010000000100000001000000010000000000000000000000000000000000000001000000010000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/Root
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.L
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.L/Upper_Arm.L
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.L/Upper_Arm.L/Lower_Arm.L
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.L/Upper_Arm.L/Lower_Arm.L/Wrist.L
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.L/Upper_Arm.L/Lower_Arm.L/Wrist.L/Upper_Hand.L
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.L/Upper_Arm.L/Lower_Arm.L/Wrist.L/Upper_Hand.L/Lower_Hand.L
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.L/Upper_Arm.L/Lower_Arm.L/Wrist.L/Upper_Thumb.L
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.L/Upper_Arm.L/Lower_Arm.L/Wrist.L/Upper_Thumb.L/Lower_Thumb.L
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.R
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.R/Upper_Arm.R
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.R/Upper_Arm.R/Lower_Arm.R
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.R/Upper_Arm.R/Lower_Arm.R/Wrist.R
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.R/Upper_Arm.R/Lower_Arm.R/Wrist.R/Upper_Hand.R
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.R/Upper_Arm.R/Lower_Arm.R/Wrist.R/Upper_Hand.R/Lower_Hand.R
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.R/Upper_Arm.R/Lower_Arm.R/Wrist.R/Upper_Thumb.R
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Shoulder.R/Upper_Arm.R/Lower_Arm.R/Wrist.R/Upper_Thumb.R/Lower_Thumb.R
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Head
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Head/Lower_Ear.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Head/Lower_Ear.L/Upper_Ear.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Head/Lower_Ear.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Head/Lower_Ear.R/Upper_Ear.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Upper_Leg.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Upper_Leg.L/Lower_Leg.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Upper_Leg.L/Lower_Leg.L/Foot_Roll.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Upper_Leg.L/Lower_Leg.L/Foot_Roll.L/Foot.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Upper_Leg.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Upper_Leg.R/Lower_Leg.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Upper_Leg.R/Lower_Leg.R/Foot_Roll.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Upper_Leg.R/Lower_Leg.R/Foot_Roll.R/Foot.R
    m_Weight: 1
  - m_Path: GDW_Goblin_Body
    m_Weight: 0
  - m_Path: GDW_Goblin_Eyes
    m_Weight: 0
  - m_Path: GDW_Goblin_Hands
    m_Weight: 0
  - m_Path: GDW_Goblin_Horns
    m_Weight: 0
  - m_Path: GDW_Goblin_Leg
    m_Weight: 0
  - m_Path: GDW_Goblin_Pants
    m_Weight: 0
  - m_Path: GDW_Goblin_Teeth
    m_Weight: 0
  - m_Path: GDW_Goblin_Tongue
    m_Weight: 0
