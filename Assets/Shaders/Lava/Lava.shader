﻿Shader "Tisch4/Lava"
{
    Properties
    {            
        [Header(Color)]
        [Space(6)]
        _ColorBase("Base", color) = (0, 0, 0, 0)
        _ColorAccent("Accent", Color) = (0.5, 0.5, 0.5, 1)
        _ColorOffset("Offset", float) = 1
        _ColorStrength("Strength", Range(0, 1)) = 1
        
        [Header(Edge falloff)]
        [Space(6)]
        _EdgeColor("Color", Color) = (1, 1, 1, 1)
        _EdgeValue("Value", Float) = 1
        _EdgeBlur("Blur", Float) = 1
        _EdgeStrength("Strength", Float) = 1
        
        [Header(Highlights)]
        [Space(6)]
        _HighlightColor("Color", Color) = (1, 1, 1, 1)
        _HighlightCutoff("Cutoff", Float) = 1
        _HighlightBlur("Blur", Float) = 1
        _HighlightStrength("Strength", Float) = 1
        
        [Header(Mask)]
        [Space(6)]
        _MaskTex ("Texture (2D)", 2D) = "white" {}
        _MaskDirectionX("Direction X", Range(-1, 1)) = 1
        _MaskDirectionY("Direction Y", Range(-1, 1)) = 1
        _MaskSpeed("Speed", float) = 1

        [Header(Distortion)]
        [Space(6)]
        _DistortionTex("Texture (2D)", 2D) = "white"{}
        _DistortionStrength("Strength", Range(0, 1)) = 1
        
        [Header(Noise 1)]
        [Space(6)]
        _Noise1DirectionX("Direction X", Range(-1, 1)) = 1
        _Noise1DirectionY("Direction Y", Range(-1, 1)) = 1
        _Noise1Speed("Speed", Float) = 1
        
        [Header(Noise 2)]
        [Space(6)]
        _Noise2DirectionX("Direction X", Range(-1, 1)) = 1
        _Noise2DirectionY("Direction Y", Range(-1, 1)) = 1
        _Noise2Speed("Speed", Float) = 1
        
        [Header(Wave)]
        [Space(6)]
        _WaveAmount("Amount", Float) = 1
        _WaveHeight("Height", Float) = 0.1
        _WaveSpeed("Speed", Float) = 1
    }
    SubShader
    {
        Tags
        {
            "RenderType"="Opaque"
        }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            #pragma shader_feature_local UVSPACE

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 color : COLOR;
                float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD2;
                float eyeDepth: TEXCOORD3;
            };

            float _UvSpace;

            fixed4 _ColorBase, _ColorAccent;
            float _ColorOffset, _ColorStrength;

            fixed4 _EdgeColor;
            float _EdgeValue, _EdgeBlur, _EdgeStrength;

            fixed4 _HighlightColor;
            float _HighlightCutoff, _HighlightBlur, _HighlightStrength;

            sampler2D _MaskTex;
            float4 _MaskTex_ST;
            float _MaskDirectionX, _MaskDirectionY;
            float _MaskSpeed;

            sampler2D _DistortionTex;
            float4 _DistortionTex_ST;
            float _DistortionStrength;

            float _Noise1DirectionX, _Noise1DirectionY;
            float _Noise1Speed;

            float _Noise2DirectionX, _Noise2DirectionY;
            float _Noise2Speed;

            float _WaveAmount, _WaveHeight, _WaveSpeed;

            sampler2D _CameraDepthTexture;

            v2f vert(appdata v)
            {
                v2f o;
                v.vertex.y += sin(_Time.z * _WaveSpeed + (v.vertex.x * v.vertex.z * _WaveAmount)) * _WaveHeight * v.color.r;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = mul(unity_ObjectToWorld, v.vertex).xz;
                o.color = v.color;
                o.screenPos = ComputeScreenPos(o.vertex);
                o.eyeDepth = COMPUTE_EYEDEPTH(v.vertex);
                UNITY_TRANSFER_FOG(o, o.vertex);
                return o;
            }

            float SampleNoise(float2 uvs)
            {
                float2 uv = TRANSFORM_TEX(uvs, _DistortionTex);
                
                float2 noise1Uv = uv + _Time.xx * float2(_Noise1DirectionX, _Noise1DirectionY) * _Noise1Speed;
                float noise1 = tex2D(_DistortionTex, noise1Uv * 0.5).r;

                float2 noise2Uv = uv + _Time.xx * float2(_Noise2DirectionX, _Noise2DirectionY) * _Noise2Speed;
                float noise2 = tex2D(_DistortionTex, noise2Uv).r;

                return saturate((noise1 + noise2) * 0.5);
            }

            fixed4 SampleMask(float2 uvs, float multi, float2 offset)
            {
                float2 uv = TRANSFORM_TEX(uvs, _MaskTex);
                uv += _Time.xx * float2(_MaskDirectionX, _MaskDirectionY) * _MaskSpeed;
                uv += multi;
                return tex2D(_MaskTex, uv + (offset * _DistortionStrength));
            }

            float SampleDepth(float4 screenPos, float eyeDepth){                
                float rawDepth = SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(screenPos));
                
                float persp = LinearEyeDepth(rawDepth);
                float ortho = (_ProjectionParams.z - _ProjectionParams.y) * (1 - rawDepth) + _ProjectionParams.y;
                float depth = lerp(persp, ortho ,unity_OrthoParams.w);
    
                float diff = depth - eyeDepth;
                
                return diff;
            }

            void Edge(inout fixed4 color, fixed4 col, float depth, float multi)
            {
                float edge = smoothstep(1 - col, 1 - col + _EdgeBlur, 1 - saturate(_EdgeValue * depth)) * multi;
                
                color *= 1 - edge;
                color += (edge * _EdgeColor) * _EdgeStrength;
            }

            void Highlights(inout fixed4 color, fixed4 col, float multi)
            {
                float highlight = smoothstep(_HighlightCutoff, _HighlightCutoff + _HighlightBlur, col) * multi;
                
                color *= 1 - highlight;
                color += highlight * _HighlightColor * _HighlightStrength;
            }


            fixed4 frag(v2f i) : SV_Target
            {                
                float noise = SampleNoise(i.uv);
                float mask = SampleMask(i.uv, i.color.r, noise).r;
                float depth = SampleDepth(i.screenPos, i.eyeDepth);

                fixed4 col = noise + (mask * i.color.r);  
                fixed4 color = lerp(_ColorBase, _ColorAccent, col * _ColorOffset) * _ColorStrength;

                Edge(color, col, depth, 1);
                Highlights(color, col, i.color.r);
                
                color *= i.color.r;

                UNITY_APPLY_FOG(i.fogCoord, col);
                return color;
            }
            ENDCG
        }
    }
}