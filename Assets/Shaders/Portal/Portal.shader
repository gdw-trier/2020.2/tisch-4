﻿Shader "Tisch4/Portal"
{
    Properties
    {    
        [Header(Noise)]
        [Space(6)]
        _NoiseSwirlColor("Swirl Color", Color) = (1, 0, 0, 1)
        _NoiseGlowColor("Glow Color", Color) = (1, 0, 0, 1)
        _NoiseEmission("Emission", Float) = 10
        _NoiseTex("Texture (R)", 2D) = "white"{}
        
        [Header(Refraction)]
        [Space(6)]
        _RefractionTex("Texture (Bump)", 2D) = "bump"{}
        _RefractionStrength("Strength", Float) = 1 
        
        [Header(Twirl)]
        [Space(6)]
        _TwirlStrength("Strength", Float) = 1
        _TwirlSpeed("Speed", Float) = 1
        
        [Header(Mask)]
        [Space(6)]
        _MaskTex("Mask (R)", 2D) = "white"{}
        _MaskScale("Scale", Float) = 1
        _MaskSmoothness("Smoothness", Float) = 1
        _MaskCenterX("Center X", Range(-1, 1)) = 0.5
        _MaskCenterY("Center Y", Range(-1, 1)) = 0.5
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100
        Cull off
        
        GrabPass
        {
            "_GrabTexture"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #define SCALE 100

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float4 grabUV : TEXCOORD1;
            };

            sampler2D _GrabTexture;
            
            sampler2D _MaskTex;
            float _MaskScale, _MaskSmoothness;
            float _MaskCenterX, _MaskCenterY;
            
            sampler2D _NoiseTex;
            float4 _NoiseTex_ST;
            fixed4 _NoiseSwirlColor, _NoiseGlowColor;
            float _NoiseEmission;

            sampler2D _RefractionTex;
            float _RefractionStrength;

            float _TwirlStrength, _TwirlSpeed;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.grabUV = ComputeGrabScreenPos(o.vertex);
                return o;
            }

            float2 Twirl(float2 UV, float2 Center, float Strength, float2 Offset)
            {
                float2 delta = UV - Center;
                float angle = Strength * length(delta);
                float x = cos(angle) * delta.x - sin(angle) * delta.y;
                float y = sin(angle) * delta.x + cos(angle) * delta.y;
                return float2(x + Center.x + Offset.x, y + Center.y + Offset.y);
            }

            float Circle(in float2 uv, in float2 center, in float radius, float smoothness){
                float2 dist = uv-center;
	            return 1.-smoothstep(radius-(radius*smoothness),
                                     radius+(radius*smoothness),
                                     dot(dist,dist)*4.0);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                
                float2 uv = Twirl(i.uv, 0.5, _TwirlStrength, (_Time.x * 0.5) * _TwirlSpeed);

                //float mask = pow(tex2D(_MaskTex, i.uv).r, _MaskScale);
                float mask = Circle(i.uv, float2(_MaskCenterX, _MaskCenterY), _MaskScale, _MaskSmoothness);
                float noise = tex2D(_NoiseTex, uv) * mask;
                half3 map = UnpackNormal(tex2D(_RefractionTex, uv));
                half3 offset = map * (_RefractionStrength / SCALE) * mask;

                fixed4 col = tex2Dproj(_GrabTexture, i.grabUV + half4(offset, 0));
                col += _NoiseGlowColor * mask;
                col += noise * _NoiseSwirlColor * _NoiseEmission;
                return col;
            }
            ENDCG
        }
    }
}
