﻿Shader "Tisch4/Dissolve"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        
        [Header(Dissolve)]
        [Space(6)]
        _DissolveAmount("Amount", Range(0, 1)) = 0
        _DissolveTex("Texture (2D)", 2D) = "white"{}
        
        [Header(Edge)]
        [Space(6)]
        _EdgeSize("Size", Float) = 0.05
        _EdgeColor("Color", Color) = (1, 0, 0, 1)
        _EdgeStrength("Strength", Float) = 20
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows addshadow

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_DissolveTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        sampler2D _DissolveTex;
        float _DissolveAmount;

        float _EdgeSize, _EdgeStrength;
        fixed4 _EdgeColor;

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            float t = tex2D(_DissolveTex, IN.uv_DissolveTex) - _DissolveAmount;
            clip(t);

            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;

            if(_DissolveAmount > 0)
            {
                o.Emission = tex2D(_DissolveTex, float2(t * (1 / _EdgeSize), 0)) * _EdgeColor * _EdgeStrength * step(t, _EdgeSize);
            }
        }
        ENDCG
    }
    FallBack "Diffuse"
}
