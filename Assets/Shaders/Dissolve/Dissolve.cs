﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Utilities;

namespace Shaders.Dissolve{
    public class Dissolve : MonoBehaviour{
        public UnityEvent onFadeOut, onFadeIn;

        private MaterialPropertyBlock _PropertyBlock;
        private Timer _FadeOutTimer, _FadeInTimer;
        private static readonly int _DissolveAmountId = Shader.PropertyToID("_DissolveAmount");

        private bool _Fading;

        private List<Renderer> _Renderers;

        private void Awake(){
            _PropertyBlock = new MaterialPropertyBlock();
            _FadeOutTimer = new Timer(0f, 1f);
            _FadeInTimer = new Timer(1f, 0f);

            _Renderers = GetRenderer();
        }

        private void OnEnable(){
            _FadeOutTimer.OnUpdate += UpdateDissolve;
            _FadeInTimer.OnUpdate += UpdateDissolve;

            _FadeOutTimer.OnEnd += OnFadOutEnd;
            _FadeInTimer.OnEnd += OnFadeInEnd;
        }

        private void OnDisable(){
            _FadeOutTimer.OnUpdate -= UpdateDissolve;
            _FadeInTimer.OnUpdate -= UpdateDissolve;

            _FadeOutTimer.OnEnd -= OnFadOutEnd;
            _FadeInTimer.OnEnd -= OnFadeInEnd;
        }

        private List<Renderer> GetRenderer(){
            List<Renderer> list = new List<Renderer>(transform.childCount);
            for (int i = 0; i < transform.childCount; ++i){
                var child = transform.GetChild(i);
                if (!child.gameObject.TryGetComponent(out Renderer render)) continue;

                if (!render.sharedMaterial.HasProperty(_DissolveAmountId)) continue;

                list.Add(render);
            }

            return list;
        }

        private void UpdateDissolve(float t){
            if (_Renderers.Count <= 0 || _Renderers == null) return;

            foreach (var r in _Renderers){
                r.GetPropertyBlock(_PropertyBlock);
                _PropertyBlock?.SetFloat(_DissolveAmountId, t);
                r.SetPropertyBlock(_PropertyBlock);
            }
        }

        private void OnFadOutEnd(){
            _Fading = false;
            onFadeOut?.Invoke();
        }

        private void OnFadeInEnd(){
            _Fading = false;
            onFadeIn?.Invoke();
        }

        public void FadeOut(float t){
            if (_Fading) return;

            _Fading = true;
            StartCoroutine(_FadeOutTimer.Run(t));
        }

        public void FadeIn(float t){
            if (_Fading) return;

            _Fading = true;
            StartCoroutine(_FadeInTimer.Run(t));
        }
    }
}