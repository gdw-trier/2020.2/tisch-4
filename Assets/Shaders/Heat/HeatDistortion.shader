﻿Shader "Hidden/Tisch4/Heat Distortion"
{
     HLSLINCLUDE

        #include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"

        TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
        TEXTURE2D_SAMPLER2D(_NoiseTex, sampler_NoiseTex);
        float _Strength;
        float _Speed;

        #define SCALE 100

        float4 Frag(VaryingsDefault i) : SV_Target
        {
            float2 uv = i.texcoord + float2(0, -_Time.y * (_Speed / SCALE));
            float noise = SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, uv) * (_Strength / SCALE);
            return SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoord + noise);
        }

    ENDHLSL

    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            HLSLPROGRAM

                #pragma vertex VertDefault
                #pragma fragment Frag

            ENDHLSL
        }
    }
}