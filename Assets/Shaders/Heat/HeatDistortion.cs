﻿using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace Shaders.Heat{
    [Serializable, PostProcess(typeof(HeatDistortionRenderer), PostProcessEvent.AfterStack, "Tisch4/Heat Distortion")]
    public sealed class HeatDistortion : PostProcessEffectSettings{
        public FloatParameter strength = new FloatParameter{value = 1f};
        public FloatParameter speed = new FloatParameter{value = 1f};
        public TextureParameter noise = new TextureParameter{defaultState = TextureParameterDefault.Black};
    }

    public sealed class HeatDistortionRenderer : PostProcessEffectRenderer<HeatDistortion>{
        private static readonly int _Strength = Shader.PropertyToID("_Strength");
        private static readonly int _Noise = Shader.PropertyToID("_NoiseTex");
        private static readonly int _Speed = Shader.PropertyToID("_Speed");

        public override void Render(PostProcessRenderContext context){
            var sheet = context.propertySheets.Get(Shader.Find("Hidden/Tisch4/Heat Distortion"));
            sheet.properties.SetFloat(_Strength, settings.strength);
            sheet.properties.SetFloat(_Speed, settings.speed);
            settings.noise.value = settings.noise.value == null 
                ? settings.noise.value = Texture2D.blackTexture 
                : settings.noise.value;
            sheet.properties.SetTexture(_Noise, settings.noise);
            context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
        }
    }
}