﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontShowInGame : MonoBehaviour
{
    // Disables the renderer to hide objects in game
    void Start()
    {
        Renderer renderer;
        if (TryGetComponent<Renderer>(out renderer))
        {
            renderer.enabled = false;
        }
    }
}
