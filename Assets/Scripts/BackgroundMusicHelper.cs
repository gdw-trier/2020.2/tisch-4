﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusicHelper : MonoBehaviour
{
    [SerializeField] private AudioSource ambientSource;
    
    public void OnGameStart()
    {
        ambientSource.Play();
    }
}
