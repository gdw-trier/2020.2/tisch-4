﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Systems.Checkpoint;
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class GameMasterController : MonoBehaviour
{
    #region Controls
    [SerializeField] private float cursorSpeed = 1f;
    
    private Transform cursorTf;
    private SpriteRenderer cursorRenderer;

    #endregion
    
    #region Camera
    [SerializeField] private float camSpeed = 1f;
    
    [SerializeField] private List<string> ignoredLayers;
    
    private float currentScrollStep;
    private float scrollStepSize;
    
    [SerializeField] private float minFieldOfView = 45f;
    [SerializeField] private float maxFieldOfView = 90f;
    [SerializeField] private int scrollSteps;
    [SerializeField] private float zoomSpeedGamepad = 0.05f;
    [SerializeField] private float zoomSpeedScrollWheel = 1f;
    
    private Transform camTargetTf;
    private Vector3 camMovement, cursorMovement;
    #endregion

    #region References

    [SerializeField] private CinemachineVirtualCamera _virtualCamera;
    [SerializeField] private Camera _cam;
    
    [SerializeField] private InputActionAsset actionMap;
    [SerializeField] private GameObject GM_CamTargetPrefab;
    [SerializeField] private GameObject GM_CursorPrefab;
    [SerializeField] private GameObject GM_BuildMenuPrefab;
    
    #endregion
    
    [HideInInspector] public bool usesKBM = false;
    private InputAction mouseCursor;
    private InputAction zoomAction;
    private bool gameStarted = false;
    
    private GM_UIController gmUIController;
    
    [SerializeField] private List<GM_BuildAbility> BuildAbilities;
    private List<GM_BuildAbility> availableAbilities;
    public PauseMenu pauseMenu;

    private GM_BuildAbility currentAbility;
    private GM_BuildAbility CurrentAbility
    {
        get
        {
            return currentAbility;
        }
        set
        {
            gmUIController.OnAbilityChanged(value);
            currentAbility = value;

            if (!currentAbility)
            {
                buildGameObject = null;
                buildTf = null;
                buildHelper = null;
                
                return;
            }
            
            buildGameObject = Instantiate(currentAbility.trapPreviewPrefab, cursorTf.position + currentAbility.customOffset,
                Quaternion.identity);

            buildTf = buildGameObject.transform;
            buildHelper = buildGameObject.GetComponent<GM_BuildableHelper>();
        }
    }

    private GameObject buildGameObject;
    private Transform buildTf;
    private MeshRenderer buildRenderer;
    private GM_BuildableHelper buildHelper;
    
    [SerializeField][Tooltip("How often the player gets a new ability")]
    private float grantCooldown = 5f;
    
    [SerializeField]
    private float onReceiveCooldown = 5f;
    
    private List<int> abilityLootTable;
    private int totalLootChance;

    private bool usedJoinLobby;

    private void Start()
    {
        camTargetTf = Instantiate(GM_CamTargetPrefab).transform;
        _virtualCamera.Follow = camTargetTf;
        _virtualCamera.LookAt = camTargetTf;

        cursorTf = Instantiate(GM_CursorPrefab).transform;
        cursorRenderer = cursorTf.GetComponent<SpriteRenderer>();
        
        cursorTf.gameObject.SetActive(!usedJoinLobby);

        mouseCursor = actionMap.FindAction("GM_CursorMouse");
        zoomAction = actionMap.FindAction("GM_Zoom");

        Canvas canvas = null;
        Canvas[] foundCanvases = FindObjectsOfType<Canvas>();
        foreach (var foundCanvas in foundCanvases)
        {
            if (foundCanvas.CompareTag("IngameCanvas"))
            {
                canvas = foundCanvas;
            }
        }

        if (canvas)
        {
            Transform canvasTf = canvas.transform;
            gmUIController = Instantiate(GM_BuildMenuPrefab, canvasTf).GetComponent<GM_UIController>();
        }

        transform.position = Vector3.zero;
        transform.rotation = Quaternion.Euler(0f, 145f, 0f);
        
        camTargetTf.position = new Vector3(-50f, 0f, 70f);
        cursorTf.position = camTargetTf.position + Vector3.up * 10f;

        scrollStepSize = (maxFieldOfView - minFieldOfView) / scrollSteps;
        _virtualCamera.m_Lens.FieldOfView = maxFieldOfView;
        currentScrollStep = scrollSteps;
        
        if(gmUIController)
            gmUIController.gameObject.SetActive(false);

        totalLootChance = 0;
        abilityLootTable = new List<int>();
        
        foreach (GM_BuildAbility buildAbility in BuildAbilities)
        {
            abilityLootTable.Add(totalLootChance + buildAbility.dropRateInPercent);
            totalLootChance += buildAbility.dropRateInPercent;
        }
        
        availableAbilities = new List<GM_BuildAbility>();
        
        InvokeRepeating(nameof(GrantAbility), grantCooldown / 2f, grantCooldown);
    }

    private void Update()
    {
        if(!gameStarted)
            return;

        if (buildGameObject)
        {
            buildTf.position = cursorTf.position;
            if(CurrentAbility.buildType == BuildType.Lava)
                buildTf.position += CurrentAbility.customOffset;

            cursorRenderer.enabled = false;
        }
    }

    private void LateUpdate()
    {
        if(!gameStarted)
            return;
        
        #region Camera

        float minZ = (50f - -40f) * (currentScrollStep / scrollSteps) + -40f;
        float maxZ = (100f - 160f) * (currentScrollStep / scrollSteps) + 160f;
        Vector3 newTargetPos = camTargetTf.position + camMovement * (Time.deltaTime * camSpeed * 20f);
        newTargetPos.z = Mathf.Clamp(newTargetPos.z, minZ, maxZ);
        camTargetTf.position = newTargetPos;

        RaycastHit hit;
        if (usesKBM)
        {
            Vector2 mousePos = mouseCursor.ReadValue<Vector2>();
            
            Ray ray = _cam.GetComponent<Camera>().ScreenPointToRay(mousePos);

            int layerMask = LayerMask.GetMask("Default", "Lava");
            if (Physics.Raycast(ray, out hit, 1000.0f, layerMask))
            {
                cursorTf.position = hit.point + Vector3.up * 3f;
            }
        }
        else
        {
            float height = 30f;
            if(Physics.Raycast(cursorTf.position + Vector3.up * 100f, Vector3.down, out hit, 1000f, LayerMask.GetMask("Default", "Lava")))
            {
                height = hit.point.y + 3f;
            }
            
            cursorTf.position += cursorMovement * (Time.deltaTime * cursorSpeed * 20f);
            cursorTf.position = new Vector3(cursorTf.position.x, height, cursorTf.position.z);
        }
        
        if (buildHelper && hit.collider)
        {
            buildHelper.onLava = hit.collider.gameObject.layer == LayerMask.NameToLayer("Lava");
                    
            string hitName = hit.collider.name;
            buildHelper.onNormalPlatform = hit.collider.name.Equals("Platform") || 
                                           Regex.Match(hitName, @"^Platform \([0-9]{1,3}\)").Success;
        }
        #endregion
        
        UpdateZoom(zoomAction.ReadValue<Vector2>());
    }

    private void GrantAbility()
    {
        int random = Random.Range(1, totalLootChance + 1);

        int abilityIndex = 0;
        foreach (int abilityChance in abilityLootTable)
        {
            if (random <= abilityChance)
            {
                break;
            }
            else
            {
                abilityIndex++;
            }
        }

        GM_BuildAbility grantedAbility = BuildAbilities[abilityIndex];
        Debug.Log("Granted " + grantedAbility.name);
            
        availableAbilities.Add(grantedAbility);
        
        if(availableAbilities.Count > 1)
            gmUIController.OnQueueChanged(grantedAbility);
        
        if (!CurrentAbility)
            CurrentAbility = grantedAbility;
    }

    public void RefocusCamera(int layer, bool useJoinLobby)
    {
        GameObject vCam = _virtualCamera.gameObject;
        vCam.layer = layer;
        
        List<string> layers = new List<string>{"P1", "P2", "P3", "P4", "P5"};
        layers = layers.Union<string>(ignoredLayers).ToList<string>();
        
        int bitMask = ~ LayerMask.GetMask(layers.ToArray()) 
                      | (1 << layer);
        
        _cam.cullingMask = bitMask;
        vCam.layer = layer;

        this.usedJoinLobby = useJoinLobby;
        if (useJoinLobby)
        {
            _cam.enabled = false;
        }
        else
        {
            
        }
    }

    public void OnGM_Camera(InputValue value)
    {
         Vector2 input = value.Get<Vector2>();
         camMovement = new Vector3(-input.x, 0f, -input.y);
    }

    public void OnGM_MoveCursor(InputValue value)
    {
        Vector2 input = value.Get<Vector2>();
        cursorMovement = new Vector3(-input.x, 0f, -input.y);
        usesKBM = false;
    }

    private void UpdateZoom(Vector2 rawInput)
    {
        if(!gameStarted)
            return;
        
        // Scroll wheel input
        if (Mathf.Abs(rawInput.y) > 1)
        {
            currentScrollStep = Mathf.Clamp(
                currentScrollStep + zoomSpeedScrollWheel * (-rawInput.y) / 120f, 0, scrollSteps);
        }
        // Gamepad input
        else
        {
            currentScrollStep = Mathf.Clamp(
                currentScrollStep + zoomSpeedGamepad * rawInput.y, 0, scrollSteps);
        }

        _virtualCamera.m_Lens.FieldOfView = minFieldOfView + scrollStepSize * currentScrollStep;
    }

    public void OnGM_CursorMouse(InputValue value)
    {
        usesKBM = true;
    }
    
    public void OnGM_ResetCamera(InputValue value)
    {
        _virtualCamera.m_Lens.OrthographicSize = 90f;

        Vector3 targetPos = camTargetTf.position;
        
        currentScrollStep = scrollSteps;
        camTargetTf.position = new Vector3(targetPos.x, targetPos.y, 0f);
    }

    private void OnGM_Build(InputValue value)
    {
        if(!gameStarted || !CurrentAbility)
            return;

        bool buildSuccesful = false;
        
        if (buildHelper.IsBuildable)
        {
            if (CurrentAbility.buildType == BuildType.Lava)
            {
                RaycastHit hit;
                if(Physics.Raycast(cursorTf.position, Vector3.down, out hit, 1000f, LayerMask.GetMask("Lava")))
                {
                    Vector3 buildPos = hit.point + CurrentAbility.customOffset;
                    Quaternion buildRot = buildTf.rotation;

                    Instantiate(CurrentAbility.trapPrefab, buildPos, buildRot);

                    buildSuccesful = true;
                }
            }
            else
            {
                RaycastHit hit;
                if(Physics.Raycast(cursorTf.position, Vector3.down, out hit, 1000f, LayerMask.GetMask("Default")))
                {
                    Transform hitTf = hit.collider.transform;
                    Vector3 buildPos = hitTf.position + CurrentAbility.customOffset;
                    Quaternion buildRot = hitTf.rotation;

                    Destroy(hit.collider.gameObject);
                    Instantiate(CurrentAbility.trapPrefab, buildPos, buildRot);

                    buildSuccesful = true;
                }
            }
        }

        if (buildSuccesful)
        {
            Destroy(buildGameObject);
            availableAbilities.Remove(CurrentAbility);

            if (availableAbilities.Count == 0)
                CurrentAbility = null;
            else
                CurrentAbility = availableAbilities[0];

            cursorTf.GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    public void UpdateUIPosition(int splitScreens)
    {
        if(splitScreens == 1)
            return;
        
        RectTransform rectTransform = (RectTransform) gmUIController.transform;
        
        switch (splitScreens)
        {
            case 2:
                rectTransform.localScale = new Vector3(.8f, .8f, .8f);
                break;
            case 3:
            case 4:
                rectTransform.localScale = new Vector3(.45f, .45f, .45f);
                cursorTf.transform.localScale = Vector3.one * 12f;
                break;
        }
    }

    public void OnGameStart()
    {
        gameStarted = true;
        _cam.enabled = true;
        gmUIController.gameObject.SetActive(true);

        cursorTf.gameObject.SetActive(true);
        Cursor.visible = false;
        Vector3 camPos = _cam.transform.position;
        cursorTf.position = Vector3.zero;
    }

    public void OnGM_RotateClockwise(InputValue value)
    {
        if(!buildTf)
            return;
        
        buildTf.Rotate(new Vector3(0f, 90f, 0f));
    }
    
    public void OnGM_RotateCounterClockwise(InputValue value)
    {
        if(!buildTf)
            return;

        buildTf.Rotate(new Vector3(0f, -90f, 0f));
    }
    
    public void OnPauseMenu(InputValue value)
    {
        if (gameStarted)
        {
            Time.timeScale = 0f;
            Cursor.visible = true;
            pauseMenu.gameObject.SetActive(true);
        }
    }
    
    public void SplitscreenFixV2(int playerNr)
    {
        Rect camRect = _cam.rect;
        camRect.width = 0.5f;
        camRect.height = 0.5f;

        switch (playerNr)
        {
            case 0:
                camRect.x = 0.0f;
                camRect.y = 0.5f;
                break;
            case 1:
                camRect.x = 0.5f;
                camRect.y = 0.5f;
                break;
            case 2:
                camRect.x = 0.0f;
                camRect.y = 0.0f;
                break;
            case 3:
                camRect.x = 0.5f;
                camRect.y = 0.0f;
                break;
        }
        
        _cam.rect = camRect;
    }
}
