﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "BuildTrap", order = 1)]
public class GM_BuildAbility : ScriptableObject
{
    public GameObject trapPrefab;
    public GameObject trapPreviewPrefab;
    public BuildType buildType;
    public int dropRateInPercent;
    public Sprite sprite;
    public Vector3 customOffset;
}

public enum BuildType
{
    Lava, Platform
}