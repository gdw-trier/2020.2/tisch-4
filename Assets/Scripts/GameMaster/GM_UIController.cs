﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GM_UIController : MonoBehaviour
{
    [SerializeField] private Image abilityImage;
    [SerializeField] private Image cooldownImage;
    [SerializeField] private TextMeshProUGUI cooldownTMP;
    [SerializeField] private Transform abilityQueue;
    public bool showCooldown;
    private float currentCooldown;
    private List<GameObject> queuedSpells;
    private Color emptyColor;

    private void Start()
    {
        queuedSpells = new List<GameObject>();
        emptyColor = abilityImage.color;
    }

    private void Update()
    {
        if (showCooldown && currentCooldown > 0)
        {
            cooldownImage.enabled = true;
            cooldownTMP.enabled = true;
        }
    }

    public void OnAbilityChanged(GM_BuildAbility ability)
    {
        if (ability)
        {
            abilityImage.sprite = ability.sprite;
            abilityImage.color = Color.white;
        }
        else
        {
            abilityImage.sprite = null;
            abilityImage.color = emptyColor;
        }

        if (queuedSpells != null && queuedSpells.Count > 0)
        {
            GameObject spell = queuedSpells[0];
            queuedSpells.RemoveAt(0);
            Destroy(spell);
        }
    }

    public void OnQueueChanged(GM_BuildAbility ability)
    {
        GameObject enqueuedAbility = Instantiate(new GameObject(), abilityQueue);
        enqueuedAbility.name = "EnqueuedAbility";
        Image enqueueImage = enqueuedAbility.AddComponent<Image>();
        enqueueImage.sprite = ability.sprite;
        RectTransform enqueueRect = (RectTransform) enqueuedAbility.transform;
        enqueueRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 30f);
        enqueueRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 30f);
        
        queuedSpells.Add(enqueuedAbility);
    }

    public void OnCooldown(float cooldown)
    {
        currentCooldown = cooldown;
    }

    private void OnCooldownComplete()
    {
        cooldownImage.enabled = false;
    }
}
