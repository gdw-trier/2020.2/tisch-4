﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM_BuildableHelper : MonoBehaviour
{
    [SerializeField] private Material mat_build_Placeable;
    [SerializeField] private Material mat_build_notPlaceable;
    [SerializeField] private GM_BuildAbility ability;
    private MeshRenderer _renderer;

    public bool onLava;
    public bool onNormalPlatform;
    
    int _overlaps;

    private bool IsOverlapping {
        get {
            return _overlaps > 0;
        }
    }

    public bool IsBuildable
    {
        get
        {
            if(ability.buildType == BuildType.Lava)
                return !IsOverlapping && onLava;
            else
            {
                return !onLava && onNormalPlatform;
            }
        }
    }

    // Count how many colliders are overlapping this trigger.
    // If desired, you can filter here by tag, attached components, etc.
    // so that only certain collisions count. Physics layers help too.
    void OnTriggerEnter(Collider other) {
        _overlaps++;
    }

    void OnTriggerExit(Collider other) {
        _overlaps--;
    }

    private void Start()
    {
        _renderer = GetComponent<MeshRenderer>();
        if (!_renderer)
            _renderer = GetComponentInChildren<MeshRenderer>();
    }

    private void Update()
    {
        _renderer.material = IsBuildable ?  mat_build_Placeable : mat_build_notPlaceable;
    }
}
