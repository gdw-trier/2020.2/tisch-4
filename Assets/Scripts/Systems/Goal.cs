﻿using System;
using System.Collections.Generic;
using Shaders.Dissolve;
using UnityEngine;
using Utilities;

namespace Systems{
    public class Goal : MonoBehaviour{
        public float duration = 5f;

        private Timer _Timer;
        private List<int> _Index;
        private bool _Triggered;
        private Podium _Podium;

        private const string _PLAYER_TAG = "Player";

        private void Awake(){
            _Index = new List<int>(4);
            _Timer = new Timer();
            _Podium = FindObjectOfType<Podium>();
        }

        private void OnEnable(){
            _Timer.OnEnd += () => { if(_Podium)_Podium.Show(_Index); };
        }

        private void OnTriggerEnter(Collider other){
            if (!other.gameObject.CompareTag(_PLAYER_TAG) || _Triggered) return;

            _Triggered = true;
            
            if(other.gameObject.TryGetComponent(out PlayerMovement movement)){
                _Index.Add(movement.PlayerIndex);
            }

            var players = GameObject.FindGameObjectsWithTag(_PLAYER_TAG);
            foreach (var player in players){
                var dissolve = player.gameObject.GetComponentInChildren<Dissolve>();
                if (!dissolve) continue;

                dissolve.onFadeOut.AddListener(() => {
                    Destroy(player.gameObject);
                    dissolve.onFadeOut = null;
                });
                dissolve.FadeOut(duration);

                if (player.gameObject.TryGetComponent(out PlayerMovement i)){
                    if(_Index.Contains(i.PlayerIndex)) continue;
                    
                    _Index.Add(i.PlayerIndex);
                }
            }

            StartCoroutine(_Timer.Run(duration));
        }
    }
}