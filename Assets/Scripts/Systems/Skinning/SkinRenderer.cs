﻿using UnityEngine;

namespace Systems.Skinning{
    public class SkinRenderer : MonoBehaviour{
        [SerializeField] private Skin skin;
        private Renderer _Body, _Gloves, _Legs, _Pants;

        public Skin Skin{
            get => skin;
            set{
                skin = value;
                Apply();
            }
        }

#if UNITY_EDITOR
        private void OnValidate(){
            Apply();
        }
#endif
        
        private void Awake(){
           Apply();
        }

        public void Apply(){
            if (skin == null){
                Debug.Log("Skin could not be applied!");
                return;
            }
            
            Skinning(skin.body, "Body", ref _Body);
            Skinning(skin.gloves, "Gloves", ref _Gloves);
            Skinning(skin.legs, "Legs", ref _Legs);
            Skinning(skin.pants, "Pants", ref _Pants);
        }

        private void Skinning(Material mat, string id, ref Renderer render){
            render = GetRenderer(id);
            if (!render) return;

            render.material = mat;
        }

        private SkinnedMeshRenderer GetRenderer(string id){
            for (int i = 0; i < transform.childCount; ++i){
                Transform child = transform.GetChild(i);
                if (!child.gameObject.name.Contains(id)) continue;

                if (child.gameObject.TryGetComponent(out SkinnedMeshRenderer renderer)){
                    return renderer;
                }

                Debug.LogWarning($"object: {child.name} does not contain a skinnedMeshRenderer!", child.gameObject);
            }

            Debug.LogWarning($"Could not find child with name {id}");
            return null;
        }
    }
}