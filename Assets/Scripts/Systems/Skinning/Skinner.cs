﻿using UnityEngine;

namespace Systems.Skinning{
    public class Skinner : MonoBehaviour{
        [SerializeField] private Skin[] skins;

        public delegate GameObject SpawnFunction(Transform at);

        public void Spawn(Transform at, int skin, SpawnFunction toSpawn){
            if (skin < 0 || skin >= skins.Length) return;
            
            var obj = toSpawn(at);
            var render = obj.GetComponentInChildren<SkinRenderer>();
            if (!render) return;
            
            render.Skin = skins[skin];
        }
    }
}