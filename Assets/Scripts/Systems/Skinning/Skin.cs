﻿using UnityEngine;

namespace Systems.Skinning{
    [CreateAssetMenu(fileName = "new Skin", menuName = "Tisch4/Skin", order = 0)]
    public class Skin : ScriptableObject{
        public Material body, gloves, legs, pants;
    }
}