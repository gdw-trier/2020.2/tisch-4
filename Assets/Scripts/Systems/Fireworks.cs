﻿using UnityEngine;
using UnityEngine.Events;

namespace Systems{
    [RequireComponent(typeof(Collider))]
    public class Fireworks : MonoBehaviour{
        public UnityEvent onFireworks;
        
        private void Awake(){
            GetComponent<Collider>().isTrigger = true;
        }

        private void OnTriggerEnter(Collider other){
            onFireworks?.Invoke();
        }
    }
}