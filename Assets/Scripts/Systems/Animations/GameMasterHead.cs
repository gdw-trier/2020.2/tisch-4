﻿using UnityEngine;

namespace Systems.Animations{
    public class GameMasterHead : MonoBehaviour{
        [Range(0, 1)] public float smoothness = 0.1f;

        private void LateUpdate(){
            UpdateTransform();
        }

        private void UpdateTransform(){
            var target = TargetPosition();
            var targetPosition = transform.position + Vector3.up * Mathf.Sin(Time.time);
            transform.position = Vector3.Lerp(transform.position, targetPosition, smoothness);

            var targetRotation = Quaternion.LookRotation(target - transform.position, Vector3.up);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, smoothness);
        }

        private Vector3 TargetPosition(){
            var players = PI_Manager.Instance.Players;
            if (players.Count <= 0) return transform.position;            
            
            Vector3 position = Vector3.zero;
            foreach (var player in players){
                if(player)
                    position += player.transform.position;
            }

            return position / players.Count;
        }
    }
}