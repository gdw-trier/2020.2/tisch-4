﻿using System;
using UnityEngine;

namespace Systems.Animations{
    [RequireComponent(typeof(PlayerMovement))]
    public class PlayerAnimation : MonoBehaviour{
        private Animator _Animator;
        private PlayerMovement _Movement;

        private bool _Enabled;
        
        private static readonly int _Velocity = Animator.StringToHash("Velocity");
        private static readonly int _Jump = Animator.StringToHash("Jump");
        private static readonly int _Attack = Animator.StringToHash("Attack");
        private static readonly int _Grounded = Animator.StringToHash("Grounded");

        public void Awake(){
            _Animator = GetComponentInChildren<Animator>();
            _Movement = GetComponent<PlayerMovement>();

            _Enabled = _Animator != null;
        }

        private void OnEnable(){
            if (!_Enabled) return;
            _Movement.onJump += OnJump;
            _Movement.onPunch += OnPunch;
        }

        private void OnDisable(){
            if (!_Enabled) return;
            _Movement.onJump -= OnJump;
            _Movement.onPunch -= OnPunch;
        }

        private void Update(){
            if (!_Enabled) return;
            
            _Animator.SetBool(_Grounded, _Movement.isGrounded);
            
            _Animator.SetFloat(_Velocity, _Movement.moveInput.magnitude);
        }

        private void OnJump(){
            _Animator.SetTrigger(_Jump);
        }
        
        private void OnPunch(){
            _Animator.SetTrigger(_Attack);
        }
    }
}