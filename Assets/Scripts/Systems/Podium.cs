﻿using System;
using System.Collections.Generic;
using Systems.Skinning;
using Shaders.Dissolve;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utilities;

namespace Systems{
    public class Podium : MonoBehaviour{
        public float secondsOfGlory = 30f;
        public GameObject goblinPrefab;
        public Transform[] spawns;
        [SerializeField] private TextMeshProUGUI winnerTxt;

        private Camera _Camera;
        private Camera _mainCamera;
        private Skinner _Skinner;
        private Timer _Timer;

        private void Awake(){
            _Camera = GetComponentInChildren<Camera>();
            _Camera.gameObject.SetActive(false);

            _Skinner = GetComponent<Skinner>();
            _Timer = new Timer();

            foreach (Camera cam in FindObjectsOfType<Camera>())
            {
                if (cam.CompareTag("MainCamera"))
                {
                    _mainCamera = cam;
                }
            }
        }

        private void OnEnable(){
            _Timer.OnEnd += OnPodiumEnd;
        }

        private void OnDisable(){
            _Timer.OnEnd -= OnPodiumEnd;
        }

        public void Show(List<int> index)
        {
            _mainCamera.enabled = false;
            PI_Manager.Instance.PlayerInputManager.DisableJoining();
            _Camera.gameObject.SetActive(true);
            _Camera.enabled = true;

            for (int i = 0; i < index.Count; i++){
                if(!spawns[i]) continue;
                
                _Skinner.Spawn(spawns[i], index[i], CreatePrefab);
            }

            if (index.Count == 0)
            {
                winnerTxt.text = "GM wins.";
            }
            else
            {
                winnerTxt.text = "Player " + (index[0] + 1) + " wins.";
            }

            StartCoroutine(_Timer.Run(secondsOfGlory));
        }

        private GameObject CreatePrefab(Transform at){
            var goblin = Instantiate(goblinPrefab, at);
            if (goblin.TryGetComponent(out Rigidbody rigidbody)){
                rigidbody.isKinematic = true;

                var dissolve = goblin.GetComponentInChildren<Dissolve>();
                if (dissolve){
                    dissolve.onFadeIn.AddListener(() => {
                        rigidbody.isKinematic = false;
                        dissolve.onFadeIn = null;
                    });

                    dissolve.FadeIn(3f);
                }
            }

            return goblin;
        }

        private void OnPodiumEnd()
        {
            Cursor.visible = true;
            SceneManager.LoadScene("MainScene");
        }
    }
}