﻿using System;
using System.Collections;
using System.Collections.Generic;
using Systems.Checkpoint;
using UnityEngine;

public class ResetTrigger : MonoBehaviour
{
    private Tracker _tracker;
    

    // resets every gameobject that has an tracker attached to it 
    // only works if the objects actually collide
    private void OnCollisionEnter(Collision other)
    {
        //Debug.Log("collision: true \nreset: " + other.gameObject.TryGetComponent(out toReset));
        if (other.gameObject.TryGetComponent(out _tracker))
        {
            Debug.Log("Trigger reacted to collision of: " + other.gameObject.name + " with " + gameObject.name);
            _tracker.Spawn();
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("collision: true \nreset: " + other.gameObject.TryGetComponent(out toReset));
        if (other.gameObject.TryGetComponent(out _tracker))
        {
            Debug.Log("Trigger reacted to collision of: " + other.gameObject.name + " with " + gameObject.name);
            _tracker.Spawn();
        }

    }
}
