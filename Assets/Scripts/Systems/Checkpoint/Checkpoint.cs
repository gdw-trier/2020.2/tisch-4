﻿using System;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;

namespace Systems.Checkpoint{
    [RequireComponent(typeof(Collider))]
    public class Checkpoint : MonoBehaviour{
        public LayerMask playerMask;
        private Collider _Collider;

        public bool defunct;
        
        private void Awake(){
            _Collider = GetComponent<Collider>();
            _Collider.isTrigger = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            defunct = other.gameObject.TryGetComponent(out RisingLava lava);
            Debug.Log(other.gameObject.name + " set defunct to: " + defunct);
            
            if (!other.gameObject.TryGetComponent(out Tracker tracker)) return;
            
            tracker.Checkpoint = this;
            
             Debug.Log("checkpoint set to: " + other.gameObject.GetComponent<Tracker>().Checkpoint);
        }

        private void OnCollisionEnter(Collision other)
        {
            defunct = other.gameObject.TryGetComponent(out RisingLava lava);
            Debug.Log(other.gameObject.name + " set defunct to: " + defunct);
        }

        /// <summary>
        /// Get spawn position. Position is random point on xz plane based on bounds of collider
        /// </summary>
        public Vector3 SpawnPosition{
            get{
                var bounds = _Collider.bounds;
                var x = transform.forward * bounds.extents.x * Random.Range(-1f, 1f);
                var y = transform.up * bounds.extents.y;
                var z = transform.right * bounds.extents.z * Random.Range(-1f, 1f);
                return bounds.center + x + y + z;
            }
        }

        /// <summary>
        /// Get spawn orientation.
        /// </summary>
        public Quaternion SpawnRotation => transform.rotation;
    }
}