﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace Systems.Checkpoint{
    /// <summary>
    /// Tracked which checkpoint a player has reached.
    /// Can be used for respawning players on death.
    /// </summary>
    public class Tracker : MonoBehaviour{
        /// <summary>Get last checkpoint</summary>
        public Checkpoint Checkpoint{ get; set; }

        private RisingLava lava;

        private void Start()
        {
            lava = GameObject.FindWithTag("Lava").GetComponent<RisingLava>();
        }

        [ContextMenu("Spawn")]
        public void Spawn(){
            
            if (Checkpoint.defunct)
            {
                Debug.Log("destroy: " + gameObject.name);
                Destroy(gameObject);
            }
            
            transform.position = Checkpoint.SpawnPosition;
            transform.rotation = Checkpoint.SpawnRotation;

            PlayerMovement playerMovement = GetComponent<PlayerMovement>();
            if (playerMovement)
            {
                playerMovement.OnRespawn();
            }
        }
        
        
    }
}
