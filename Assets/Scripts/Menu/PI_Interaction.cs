﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PI_Interaction : MonoBehaviour
{
    public GameObject playerInputManager;

    private PI_Manager _manager;

    private void Start()
    {
        _manager = playerInputManager.GetComponent<PI_Manager>();
    }
    
    public void SetUdeGameMaster(bool use)
    {
        _manager.SetUseGameMaster(use);
    }

    public void OnGameStart()
    {
        _manager.OnGameStart();
    }

    public void EnableJoining()
    {
        _manager.PlayerInputManager.EnableJoining();
    }

    public bool GetUseGameMaster()
    {
        return _manager.useGameMaster;
    }
}
