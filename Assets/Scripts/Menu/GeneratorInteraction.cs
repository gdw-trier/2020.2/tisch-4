﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratorInteraction : MonoBehaviour
{
    [SerializeField] private GameObject generator;
    private LevelGenerator levelGenerator;

    private void Start()
    {
        levelGenerator = generator.GetComponent<LevelGenerator>();
    }

    public void SetHeightIncrease(float height)
    {
        levelGenerator.HeightIncrease = height;
    }

    public void SetNrPlacedParts(float nrPlacedParts)
    {
        levelGenerator.NrPlacedParts = (int) nrPlacedParts;
    }

    public void SetLevelParts(List<GameObject> levelParts)
    {
        levelGenerator.LevelParts = levelParts;
    }

    public void ToggleUseUniqueOnly()
    {
        levelGenerator.UseUniqueOnly = !levelGenerator.UseUniqueOnly;
        Debug.Log("UseUniqueOnly set to: " + levelGenerator.UseUniqueOnly );
    }public void SetUseUniqueOnly(bool useUniqueOnly)
    {
        levelGenerator.UseUniqueOnly = useUniqueOnly;
        Debug.Log("UseUniqueOnly set to: " + levelGenerator.UseUniqueOnly );
    }
    
    public void RerollLevel()
    {
        Debug.Log("reroll Level");
        levelGenerator.RerollLevel();
        // SceneManager.LoadScene(levelToLoad);
    }
}
