﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaInteraction : MonoBehaviour
{
    [SerializeField] private GameObject lava;

    private RisingLava _risingLava;
    // Start is called before the first frame update
    void Start()
    {
        _risingLava = lava.GetComponent<RisingLava>();
    }

    public void SetDelay(float delay)
    {
        _risingLava.delay = delay;
    }
    
    public void SetRiseDuration(float riseDuration)
    {
        _risingLava.riseDuration = riseDuration;
    }
    
    public void SetOffset(float offset)
    {
        _risingLava.offset = offset;
    }

    public void SetHeightIncrease(float heightIncrease)
    {
        _risingLava.heightIncrease = heightIncrease;
    }

    public void SetRiseOption(bool isSupposedToRise)
    {
        _risingLava.isSupposedToRise = isSupposedToRise;
    }
    
    public void SetRise(bool isRising)
    {
        _risingLava.isRising = isRising;
    }

    public void OkToRise()
    {
        _risingLava.isRising = _risingLava.isSupposedToRise;
    }

}
