﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    
    // [SerializeField] private string levelToLoad;
    public AudioMixer mixer;

    private GameObject _reminderSettings;
    private GameObject _reminderPlay;
    private GameObject _reminderMaster;


    public void ChangeToPlayerMenu(GameObject reminderPlay)
    {
        _reminderPlay = reminderPlay;
        HideMenu(reminderPlay);
    }

    public void ChangeToMasterMenu(GameObject reminderMaster)
    {
        _reminderMaster = reminderMaster;
        HideMenu(reminderMaster);
    }

    public void ChangeToSettingsMenu(GameObject reminderSettings)
    {
        _reminderSettings = reminderSettings;
        HideMenu(reminderSettings);
    }

    public void LeftSettingsMenu(GameObject settings)
    {
        ShowMenu(_reminderSettings);
        HideMenu(settings);
    }
    public void LeftMasterMenu()
    {
        ShowMenu(_reminderMaster);
    }
    public void LeftPlayerMenu()
    {
        ShowMenu(_reminderPlay);
    }

    public void ShowMenu(GameObject menu)
    {
        menu.SetActive(true);
    }
    public void HideMenu(GameObject menu)
    {
        menu.SetActive(false);
    }
    
    public void ToggleMenu(GameObject menu)
    {
        menu.SetActive(!menu.activeSelf);
        //Debug.Log(menu + (menu.activeSelf ? " shown" : " hidden") );
    }
    
    
    public void PlayGame()
    {
        
        // Debug.Log("Play!");
        // SceneManager.LoadScene(levelToLoad);
    }

    public void QuitGame()
    {
        //Debug.Log("Quit!");
        Application.Quit();
    }

    public void SetVolume(float volume)
    {
        //Debug.Log("volume set to: " + volume);
        mixer.SetFloat("volume", volume/100f);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene("MainScene");
    }
}
