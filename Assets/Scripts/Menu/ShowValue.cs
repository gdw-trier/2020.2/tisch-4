﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShowValue : MonoBehaviour
{
    public Slider sliderUI;
    private TextMeshProUGUI _text;
    void Start()
    {
        _text = GetComponentInChildren<TextMeshProUGUI>();
        //Debug.Log("Text: " + text);
        Show();
    }

    public void Show()
    {
        _text.text = sliderUI.value + "";
    }
}
