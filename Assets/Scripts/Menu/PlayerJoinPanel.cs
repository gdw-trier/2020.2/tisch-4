﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem.Users;
using UnityEngine.UI;

public class PlayerJoinPanel : MonoBehaviour
{
    [SerializeField] private Transform content;
    [SerializeField] private GameObject joinedPlayerPrefab;
    [SerializeField] private Sprite keyboardMouseSprite;
    [SerializeField] private Sprite gamepadSprite;
    private Dictionary<InputUser, GameObject> inputUserMap;
    [SerializeField] private GameObject playButton;
    [SerializeField] private PI_Interaction _piInteraction;

    private void Start()
    {
        inputUserMap = new Dictionary<InputUser, GameObject>();
    }

    public void AddPlayer(string playerRole, string inputType, InputUser inputUser)
    {
        Transform playerInst = Instantiate(joinedPlayerPrefab, content).transform;

        if (inputType.Contains("Gamepad"))
        {
            playerInst.GetChild(0).GetComponent<Image>().sprite = gamepadSprite;
        }
        else
        {
            playerInst.GetChild(0).GetComponent<Image>().sprite = keyboardMouseSprite;
        }

        playerInst.GetComponentInChildren<TextMeshProUGUI>().text = playerRole;
        inputUserMap.Add(inputUser, playerInst.gameObject);

        int minPlayerCount = _piInteraction.GetUseGameMaster() ? 2 : 1;
        playButton.SetActive(inputUserMap.Count >= minPlayerCount);
    }

    public void RemovePlayer(InputUser inputUser)
    {
        if (inputUserMap.ContainsKey(inputUser))
        {
            Destroy(inputUserMap[inputUser]);
            inputUserMap.Remove(inputUser);
        }
        
        int minPlayerCount = _piInteraction.GetUseGameMaster() ? 2 : 1;
        playButton.SetActive(inputUserMap.Count >= minPlayerCount);
    }
}
