﻿using System;
using System.Collections;
using UnityEngine;

namespace Utilities{
    public class Timer{
        public event Action<float> OnUpdate;
        public event Action OnStart, OnEnd;

        private bool _Running;
        private float _MIN, _MAX;

        public bool Running => _Running;

        public Timer(float min = 0, float max = 1){
            _MIN = min;
            _MAX = max;
        }
        
        public IEnumerator Run(float d){
            if (_Running) yield break;

            _Running = true;
            float t = 0;
            OnStart?.Invoke();

            while (t <= 1){
                t += Time.deltaTime / d;
                OnUpdate?.Invoke(_MIN + (_MAX - _MIN) * t);
                yield return null;
            }

            OnEnd?.Invoke();
            _Running = false;
        }
    }
}