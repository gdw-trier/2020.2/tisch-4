﻿using UnityEngine;

namespace Utilities{
    [ExecuteInEditMode, RequireComponent(typeof(Camera))]
    public class DepthMode : MonoBehaviour{
        public DepthTextureMode mode;

        private void Start(){
            SetDepthTextureMode = mode;
        }

        private void OnValidate(){
            SetDepthTextureMode = mode;
        }

        public DepthTextureMode SetDepthTextureMode{
            get => GetComponent<Camera>().depthTextureMode;
            set => GetComponent<Camera>().depthTextureMode = value;
        }
    }
}