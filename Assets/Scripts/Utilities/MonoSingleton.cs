﻿using UnityEngine;

namespace Utilities{
    public class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>{
        private static T _Instance;
        private static GameObject _SingletonObject;
        private static bool _ShutDown = false;
        
        public static T Instance{
            get{
                if (_ShutDown){
                    Debug.LogWarning($"Singleton of monobehaviour {typeof(T)} is shut down. Returning null");
                    return null;
                }
                
                if (_Instance != null) return _Instance;
                
                _Instance = FindObjectOfType<T>();

                if (_Instance != null) return _Instance;
                
                _SingletonObject = new GameObject("Singletons");
                _Instance = _SingletonObject.AddComponent<T>();

                return _Instance;
            }
        }

        protected virtual void OnDisable(){
            _ShutDown = true;
        }

        protected virtual void OnEnable(){
            _ShutDown = false;
        }
    }

    public class Singleton<T> where T : Singleton<T>, new(){
        private static T _Instance;
        private static bool _ShutDown = false;

        public static T Instance{
            get{
                if (_ShutDown){
                    Debug.LogWarning($"Singleton of class {typeof(T)} is shut down. Returning null");
                    return null;
                }
                
                if (_Instance != null) return _Instance;
                
                _Instance = new T();
                return _Instance;
            }
        }

        public void Dispose(){
            _ShutDown = true;
        }
    }
}