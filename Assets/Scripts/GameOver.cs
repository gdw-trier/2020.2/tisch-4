﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    public TextMeshProUGUI gameoverText;
    
    public void OnGameOver(string message)
    {
        gameoverText.text = message;
    }
}
