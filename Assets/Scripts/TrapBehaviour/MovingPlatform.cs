﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{

    private List<Vector3> points;
    private int currentPoint = 0;
    private Vector3 currentTarget;

    public float tolerance;
    public float speed;
    public float delayTime;

    private bool stopMovement;

    // Start is called before the first frame update
    void Start()
    {
        points = new List<Vector3>();
        foreach (Transform child in transform)
        {
            if(child.name.Contains("Model"))  
                continue;
            
            points.Add(child.position);  
        }
        
        // Automatically disables component if no route was built
        if (points.Count == 0)
        {
            enabled = false;
            return;
        }

        currentPoint = 0;
        currentTarget = points[currentPoint];
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(stopMovement)
            return;
        
        Vector3 direction = currentTarget - transform.position;
        transform.position += direction.normalized * (speed * 0.01f);
        if(direction.magnitude < tolerance)
        {
            if (++currentPoint >= points.Count)
                currentPoint = 0;
            
            currentTarget = points[currentPoint];

            StartCoroutine(OnPointReached());
        }
    }

    private IEnumerator OnPointReached()
    {
        stopMovement = true;
        yield return new WaitForSeconds(delayTime);
        stopMovement = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.name.Contains("GroundTrigger")) return;

        if(other.gameObject.TryGetComponent(out RisingLava lava)) return;
        
        if(other.CompareTag("Player"))
            other.transform.parent = transform;
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.transform.name.Contains("GroundTrigger")) return;
        
        if(other.gameObject.TryGetComponent(out RisingLava lava)) return;
        
        other.transform.parent = null;
    }
}
