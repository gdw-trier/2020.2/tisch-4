﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor.UIElements;
using UnityEngine;

public class DisappearingPlatform : MonoBehaviour
{
    // This script makes the object disappear and reappear after set amounts of time
    
    // read the TOOLTIP for further information
    [Tooltip("Time the player has to react before the platform disappears")] public float delay;
    [Tooltip("Time until the platform reappears")] public float downTime;
    [Tooltip("The color the platform will change to")] public Color warningColor;

    private bool _ready = true; // keeps track of if the platform currently disappears or not

    // private Renderer[] allRenderers;
    private Rigidbody rb;
    private Vector3 defaultPos;

    private void Start()
    {
        // allRenderers = GetComponentsInChildren<Renderer>();

        rb = GetComponent<Rigidbody>();
        defaultPos = transform.position;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player") && _ready)
        {
            // only one coroutine can be started at the same time
            _ready = false;

            StartCoroutine(PlatformDrop());
        }
    }

    private IEnumerator PlatformDrop()
    {
        yield return new WaitForSeconds(delay);

        rb.useGravity = true;
        
        yield return new WaitForSeconds(downTime);

        rb.useGravity = false;
        rb.velocity = Vector3.zero;
        transform.position = defaultPos;

        _ready = true;
    }
    
    /*
    //lets the platform disappear and reappear one time
    private IEnumerator Disappear(Renderer renderer)
    {
        // remember essential components
        var collider = GetComponent<Collider>();
        var color = renderer.material.color;

        // fades the color of the platform into the preset warningcolor
        yield return renderer.material.DOColor(warningColor, delay).SetEase(Ease.Linear).WaitForCompletion();

        // make the platform disappear
        renderer.enabled = false;
        collider.enabled = false;
        
        // reset color
        renderer.material.color = color; 

        // delay before the platform reapears
        yield return new WaitForSeconds(downTime);

        // make the platform reappear
        renderer.enabled = true;
        collider.enabled = true;
        
        // ready for new cycle
        _ready = true;
    }
    */
}
