﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;


public class FireballBehaviour : MonoBehaviour
{
    // this script rotates the object it is attached to, repeatedly from an set startposition to an endpososition
    
    // lock at the TOOLTIPS for more information
    
    [Tooltip("Duration it takes to finish the Rotation")] public float duration;
    [Tooltip("Duration before the Rotation repeated")] public float downTime;
    [Tooltip("Shows the time until the next Rotation, positive while moving, negative while waiting. Can be used to create an OFFSET")] public float timer;
    [Tooltip("Relative angels to Rotate, each declared in degree measure")] public Vector3 rotation;
    
    private Vector3 _startRotation;
    private Transform _pivotTransform;

    
    // Start is called before the first frame update
    void Start()
    {
        _pivotTransform = transform.parent;
        
        if (_pivotTransform != null)
        {
            _startRotation = _pivotTransform.eulerAngles;
        }
        else
        {
            Debug.Log(gameObject.name + " has no parent and cant be rotated around it!");
        }
    }

    void Update() 
    {
        if (_pivotTransform != null)
        {
            timer -= Time.deltaTime;
            if (timer <= -downTime)
            {
                // reset to startLocation
                _pivotTransform.eulerAngles = _startRotation;
                // initiate rotation again
                _pivotTransform.DORotate(rotation, duration, RotateMode.FastBeyond360).SetRelative();
                // timer is rotation while above zero and waiting while below
                timer = duration;
            }
        }
    }
}
