﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

public class FireWallBehaviour : MonoBehaviour
{
    // this script makes the object it is attached to travel continually between two different points
    
    // read the TOOLTIPS for further information
    [Tooltip("The distance the Wall will travel relative to the current location, determents the second position")] public Vector3 distance;
    [Tooltip("The time it will take to complete the Movement")] public float moveTime;
    [Tooltip("The time the Wall stays in the first position")] public float downTime;
    [Tooltip("The time the Wall stays in the second position")] public float upTime;
    [Tooltip("Counts down the time between movement, positive while moving, negative while waiting. Can be used to create an OFFSET")] public float timer;
    
    
    // [Tooltip("Will be set automatically in the StartMethode to the current position of the object")] 
    private Vector3 _position1;
    // [Tooltip("Will be set automatically in the StartMethode, will be calculated from positon1 and distance")] 
    private Vector3 _position2;
    // [Tooltip("Keeps track of what to do next")]
    private bool _raised;

    private void Start()
    {
        _position1 = transform.position;
        _position2 = _position1 + distance;
    }

    private void ResetTimer()
    {
        timer = moveTime;
    }

    void Update()
    {
        // count down timer
        timer -= Time.deltaTime;
        
        // if timer is positive Wall is moving
        // else wall is waiting
        if (timer  <= -upTime && _raised)
        {
            //Debug.Log("upTimer: " + upTimer);

            // go to upPosition
            transform.DOMove(_position1, moveTime, false);
            
            _raised = !_raised;
            ResetTimer();
        }
        
        else if (timer <= -downTime && !_raised)
        {
            //Debug.Log("downTimer: " + timer);
                
            // go to downPosition 
            transform.DOMove(_position2, moveTime, false);
                
            _raised = !_raised;
            ResetTimer();
        }
        
    }
}
