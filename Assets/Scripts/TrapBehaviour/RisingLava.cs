﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class RisingLava : MonoBehaviour
{
    [Tooltip("Delay in between starts of lava risings, becomes meaningless if less then riseDuration")] public float delay;
    [Tooltip("The time the lava takes to get to the new level")] public float riseDuration;
    [Tooltip("Initial offset at the start of an round")] public float offset;
    [Tooltip("Initial offset at the start of an round")] public float heightIncrease;
    
    [Tooltip("Enable / disable rising lava")] [SerializeField] 
    public bool isSupposedToRise;
    
    public bool isRising;

    public bool isSynchronized;
    
    private bool isRest;
    
    private LevelGenerator generator;
    
    private float realDelay;

    private Vector3 startPos;
    
    // Start is called before the first frame update
    void Start()
    {
        isRest = true;
        
        generator = GameObject.FindWithTag("Generator")?.GetComponent<LevelGenerator>();

        startPos = transform.position;
        
    }

    public void StartRising()
    {
        StartCoroutine(RiseRoutine());
    }

    private IEnumerator RiseRoutine()
    {
        while (true)
        {
            if (isRising)
            {
                if (isRest)
                {
                    // initial offset at the start of an round
                    yield return new WaitForSeconds(offset);
                    isRest = false;
                }
                
                realDelay = delay > riseDuration ? delay - riseDuration : 0;

                // wait in between rises
                yield return new WaitForSeconds(realDelay);

                // rise lava to the next level and wait meanwhile
                yield return transform
                    .DOMove(new Vector3(0, isSynchronized ? generator.HeightIncrease : heightIncrease, 0),
                        riseDuration).SetRelative().WaitForCompletion();
            }
            else yield return null;
        }
    }

    public void Reset()
    {
        isRest = true;

        transform.position = startPos;
    }
}
