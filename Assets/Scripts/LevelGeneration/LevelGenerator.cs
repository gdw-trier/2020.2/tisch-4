﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private float heightIncrease = 5f;
    [Tooltip("Instantiates every level part only up to 1 time")]
    [SerializeField] private bool useUniqueOnly;
    [SerializeField] private int nrPlacedParts;
    [SerializeField] private List<GameObject> levelParts;
    [SerializeField] private GameObject startPrefab;
    [SerializeField] private GameObject goalPrefab;
    
    private GameObject createdLevelParts;
    private List<GameObject> chosenParts;
    private float currentOffset;
    
    public bool UseUniqueOnly
    {
        get => useUniqueOnly;
        set => useUniqueOnly = value;
    }
    public float HeightIncrease
    {
        get => heightIncrease;
        set => heightIncrease = value;
    }
    public int NrPlacedParts
    {
        get => nrPlacedParts;
        set => nrPlacedParts = value;
    }
    public List<GameObject> LevelParts
    {
        get => levelParts;
        set => levelParts = value;
    }
    
    private void Awake()
    {
            
        RerollLevel();
    }

    public void RerollLevel()
    {
        Destroy(createdLevelParts); 
        
        createdLevelParts = new GameObject("levelParts");

        currentOffset = 0f;
        
        Instantiate(startPrefab, transform.position, Quaternion.Euler(0f, -90f, 0f));
        currentOffset -= 15f;
        
        //chosenParts = levelParts;
        chosenParts = new List<GameObject>(levelParts);
        
        int placedParts = 0;
        
        for (; placedParts < nrPlacedParts; placedParts++)
        {
            GameObject placedPart = chosenParts[Random.Range(0, chosenParts.Count)];

            Vector3 spawnPos = transform.position + new Vector3(currentOffset, placedParts * heightIncrease, 0f);
            Instantiate(placedPart, spawnPos, Quaternion.identity).transform.SetParent(createdLevelParts.transform);

            currentOffset -= placedPart.GetComponent<LevelGen_Helper>().length;

            if (useUniqueOnly)
            {
                chosenParts.Remove(placedPart);

                if (chosenParts.Count == 0)
                    break;
            }
        }
        
        Vector3 goalPos = transform.position + new Vector3(currentOffset, placedParts * heightIncrease, 0f);
        Instantiate(goalPrefab, goalPos, Quaternion.Euler(0f, -90f, 0f)).transform.SetParent(createdLevelParts.transform);
        
    }

}
