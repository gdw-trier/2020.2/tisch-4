﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_Helper : MonoBehaviour
{
    private List<Vector3> spawnPositions;
    private int currentSpawn = 0;

    private void Start()
    {
        spawnPositions = new List<Vector3>();

        foreach (Transform child in transform)
        {
            spawnPositions.Add(child.position);
        }
    }

    public Vector3 GetSpawnPos()
    {
        return spawnPositions[currentSpawn++];
    }
}
