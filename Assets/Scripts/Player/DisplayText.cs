﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DisplayText : MonoBehaviour
{
    public TextMeshProUGUI playerTextMesh;

    public void SetText(string message)
    {
        playerTextMesh.enabled = true;
        playerTextMesh.text = message;
    }
}
