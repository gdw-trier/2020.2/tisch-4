﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundTrigger : MonoBehaviour
{
    private PlayerMovement playerMovement;
    private int collidedGrounds = 0;
    
    void Start()
    {
        playerMovement = GetComponentInParent<PlayerMovement>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ground"))
        {
            collidedGrounds++;

            if (collidedGrounds > 0)
            {
                playerMovement.isGrounded = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ground"))
        {
            collidedGrounds--;

            if (collidedGrounds <= 0)
            {
                playerMovement.isGrounded = false;
            }
        }
    }
}
