﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Systems.Animations;
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class PlayerMovement : MonoBehaviour
{
    #region MyRegion

    public bool usesKBM;

    #endregion
    
    #region Movement Variables
    [Header("Movement Variables")] [SerializeField]
    private float movementForce = 1f;

    [SerializeField] private float maxVelocity = 30f;
    [SerializeField] private float turnSmoothTime = 0.1f;
    [SerializeField] private float turnSmoothVelocity;

    public bool isGrounded;
    #endregion

    #region Jump Variables

    private bool jumpActionDown = false;
    
    [SerializeField] private float jumpForce = 100f;
    
    [Tooltip("Slows player movement in air")] [Min(1f)] [SerializeField]
    private float airFriction;

    [Tooltip("Additional downforce during jump")] [SerializeField]
    private float fallGravity;
    
    [Tooltip("Additional downforce when player short taps jump button")] [SerializeField]
    private float lowJumpGravity;

    #endregion

    #region Stun Variables
    
    [Header("Stun Variables")]
    [SerializeField] private float stunDuration;
    [SerializeField] private float punchHitbox;
    [SerializeField] private float punchRange = 5f;
    [SerializeField] private float stunCooldown = 3f;
    private float lastStunTime;
    
    #endregion

    #region Camera variables
    
    [Header("Camera Variables")] [SerializeField]
    private bool InvertY = false;

    [Range(1f, 20f)] [SerializeField] private float lookSpeed = 10;
    [SerializeField] private float recenteringTime = 2f;
    [SerializeField] private List<string> ignoredLayers;
    private Transform cmCamTf;
    private Camera cam;
    private Transform camTf;
    private CinemachineFreeLook freeLookComponent;
    [SerializeField] private GameObject camPrefab;
    [SerializeField] private GameObject freeLookPrefab;

    private Vector2 rawCameraInput;
    private bool recentlyRespawned;
    private bool freeMoveCamera;
    
    #endregion

    #region Component references
    private Rigidbody rb;
    private CapsuleCollider collider;
    private ParticleSystem stunEffect;
    #endregion

    private bool blockInput = false;
    public bool gameStarted = false;
    private bool firstRespawn = true;
    public int PlayerIndex{ get; set; }

    public PauseMenu pauseMenu;

    [HideInInspector] public Vector3 moveInput;
    private Vector3 camForward, camRight;

    #region Actions
    public event Action onJump, onPunch;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Player start");

        rb = GetComponent<Rigidbody>();

        
        CapsuleCollider[] colliders = GetComponents<CapsuleCollider>();
        foreach (CapsuleCollider col in colliders)
        {
            if(col.material.name.Contains("Feet"))
                collider = col;
        }

        stunEffect = GetComponentInChildren<ParticleSystem>();
        stunEffect.Stop();
        
        ParticleSystem.MainModule main = stunEffect.main;
        main.duration = stunDuration;
        main.startLifetime = stunDuration;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!gameStarted)
            return;
        
        // Improve jumping curve
        if (!isGrounded)
        {
            if (rb.velocity.y < 0f || blockInput)
            {
                rb.AddForce(Vector3.down * (fallGravity * 100f));
            }
            else if(rb.velocity.y > 0f && !jumpActionDown)
            {
                rb.AddForce(Vector3.down * (lowJumpGravity * 100f));
            }
        }

        #region Movement
        // Calculates current total (horizontal) velocity
        float currentVelocityMag = Mathf.Abs(rb.velocity.x) + Mathf.Abs(rb.velocity.z);
        
        if (!blockInput && moveInput.magnitude >= 0.1f && currentVelocityMag <= maxVelocity)
        {
            // Rotate player towards movement
            float targetAngle = Mathf.Atan2(moveInput.x, moveInput.z) * Mathf.Rad2Deg + cmCamTf.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity,
                turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            // Calculates camera relative movement direction
            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            if (isGrounded)
            {
                // Applies force to move player in desired direction
                rb.AddForce(moveDir * (movementForce * 1000f));
            }
            else
            {
                // Applies force to move player in desired direction, reduced by "airFriction" value
                rb.AddForce(moveDir * (movementForce * 1000f) / airFriction);
            }
        }
        #endregion
    }

    private void LateUpdate()
    {
        Vector2 lookMovement = Vector2.zero;
        if (usesKBM)
        {
            lookMovement = new Vector2(rawCameraInput.x / Screen.width, rawCameraInput.y / Screen.height) * 100f;
        }
        else
        {
            //Normalize the vector to have an uniform vector in whichever form it came from (I.E Gamepad, mouse, etc)
            lookMovement = rawCameraInput;
        }

        lookMovement.y = InvertY ? -lookMovement.y : lookMovement.y;

        // This is because X axis is only contains between -180 and 180 instead of 0 and 1 like the Y axis
        lookMovement.x = lookMovement.x * 180f; 

        //Ajust axis values using look speed and Time.deltaTime so the look doesn't go faster if there is more FPS
        freeLookComponent.m_XAxis.Value += lookMovement.x * (lookSpeed / 10f) * Time.deltaTime;
        freeLookComponent.m_YAxis.Value += lookMovement.y * (lookSpeed / 10f) * Time.deltaTime;

        if (!freeMoveCamera)
        {
            Vector3 localCamDirection = camTf.InverseTransformDirection(transform.forward);
            freeLookComponent.m_RecenterToTargetHeading.m_enabled = localCamDirection.z > -0.2f;
        }
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        if (blockInput)
        {
            moveInput = Vector3.zero;   
            return;
        }
        
        Vector2 rawMoveInput = context.ReadValue<Vector2>();
        moveInput = new Vector3(rawMoveInput.x, 0f, rawMoveInput.y);

        if (recentlyRespawned)
        {
            freeLookComponent.m_Heading.m_Definition = CinemachineOrbitalTransposer.Heading.HeadingDefinition.TargetForward;
            freeLookComponent.m_RecenterToTargetHeading.m_RecenteringTime = recenteringTime;

            recentlyRespawned = false;
        }

        freeMoveCamera = false;
    }

    public void OnCamera(InputAction.CallbackContext context)
    {
        if (blockInput)
        {
            rawCameraInput = Vector3.zero;
            return;
        }
        
        rawCameraInput = context.ReadValue<Vector2>();
        freeLookComponent.m_RecenterToTargetHeading.m_enabled = false;
        freeMoveCamera = true;
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        if(blockInput || !gameStarted)
            return;
        
        if (context.started)
        {
            jumpActionDown = true;
        }
        else if(context.canceled)
        {
            jumpActionDown = false;
        }

        if (isGrounded && context.started)
        {
            rb.AddForce(new Vector3(0f, jumpForce * 100f, 0f), ForceMode.Impulse);
            isGrounded = false;
            onJump?.Invoke();
        }
    }
    
    public void OnPunch(InputAction.CallbackContext context)
    { 
        if((Time.time - lastStunTime) < stunCooldown || !context.started)
            return;
        
        lastStunTime = Time.time;
        Vector3 boxCastOrigin = transform.position + new Vector3(0f, transform.localScale.y * 0.25f, 0f);
        Vector3 boxCastSize = new Vector3(punchHitbox, punchHitbox, punchHitbox) * .5f;
        RaycastHit[] hits = Physics.BoxCastAll(boxCastOrigin, boxCastSize, transform.forward,
            Quaternion.identity, punchRange, LayerMask.GetMask("Default"), QueryTriggerInteraction.UseGlobal);
        
        onPunch?.Invoke();
        if(hits.Length > 0)
        {
            foreach (RaycastHit hit in hits)
            {
                PlayerMovement playerMovement = hit.collider.GetComponent<PlayerMovement>();
                if (playerMovement && playerMovement != this)
                {    
                    
                    playerMovement.OnStunned();
                }
            }
        }
    }

    public void OnStunned()
    {
        Debug.Log(name + " was stunned.");
        rb.velocity = Vector3.zero;

        StartCoroutine(StunCo());
    }

    private IEnumerator StunCo()
    {
        blockInput = true;
        stunEffect.Play();
        yield return new WaitForSeconds(stunDuration);
        stunEffect.Stop();
        blockInput = false;
    }

    public void RefocusCamera(int layer, bool useJoinLobby)
    {
        GameObject spawnedCam = Instantiate(camPrefab);
        cam = spawnedCam.GetComponent<Camera>();
        camTf = cam.transform;
        cmCamTf = spawnedCam.transform;

        spawnedCam = Instantiate(freeLookPrefab);
        freeLookComponent = spawnedCam.GetComponent<CinemachineFreeLook>();
        freeLookComponent.Follow = transform;
        freeLookComponent.LookAt = transform;
        
        freeLookComponent.gameObject.layer = layer;
        List<string> layers = new List<string>{"P1", "P2", "P3", "P4", "P5"};
        layers = layers.Union<string>(ignoredLayers).ToList<string>();
        
        int bitMask = ~ LayerMask.GetMask(layers.ToArray()) 
                      | (1 << layer);
        
        cam.cullingMask = bitMask;
        freeLookComponent.gameObject.layer = layer;

        if (useJoinLobby)
        {
            cam.enabled = false;
            GetComponent<PlayerAnimation>().enabled = false;
        }
        else
        {
            gameStarted = true;
            cam.enabled = true;

            freeLookComponent.m_RecenterToTargetHeading.m_RecenteringTime = recenteringTime;
            freeLookComponent.m_Heading.m_Definition = CinemachineOrbitalTransposer.Heading.HeadingDefinition.TargetForward;
            freeLookComponent.m_RecenterToTargetHeading.m_enabled = true;
        }
    }

    public void OnRespawn()
    {
        float blockTime = 0.2f;
        
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        
        transform.rotation = Quaternion.Euler(0f, -90f, 0f);

        freeLookComponent.m_Heading.m_Definition = CinemachineOrbitalTransposer.Heading.HeadingDefinition.TargetForward;
        freeLookComponent.m_RecenterToTargetHeading.m_RecenteringTime = blockTime * 0.5f;

        freeLookComponent.m_YAxis.Value = 0.45f;

        recentlyRespawned = true;
        
        RaycastHit hit;
        if (!firstRespawn && Physics.Raycast(transform.position, Vector3.down, out hit, 100f, LayerMask.GetMask("Lava", "Default")))
        {
            if(hit.collider.CompareTag("Lava"))
                Destroy(gameObject);
        }
        
        firstRespawn = false;
        //freeLookComponent.m_RecenterToTargetHeading.RecenterNow();
        StartCoroutine(RespawnBlock(0.2f));
    }

    private IEnumerator RespawnBlock(float blockTime)
    {
        blockInput = true;
        yield return new WaitForSeconds(blockTime);
        blockInput = false;
    }

    public void OnGameStart()
    {
        gameStarted = true;
        cam.enabled = true;
        GetComponent<PlayerAnimation>().enabled = true;
        
        Cursor.visible = false;

        OnRespawn();
    }

    private void OnDestroy(){
        if(cam != null)Destroy(cam.gameObject);
        if(freeLookComponent != null)Destroy(freeLookComponent.gameObject);
    }
    
    public void SplitscreenFix(int players)
    {
        Rect camRect = cam.rect;

        camRect.width = 0.5f;
        if (players > 2)
        {
            camRect.height = 0.5f;
            camRect.y = 0.5f;
        }

        cam.rect = camRect;
    }
    
    public void SplitscreenFixV2(int playerNr)
    {
        Rect camRect = cam.rect;
        camRect.width = 0.5f;
        camRect.height = 0.5f;

        switch (playerNr)
        {
            case 0:
                camRect.x = 0.0f;
                camRect.y = 0.5f;
                break;
            case 1:
                camRect.x = 0.5f;
                camRect.y = 0.5f;
                break;
            case 2:
                camRect.x = 0.0f;
                camRect.y = 0.0f;
                break;
            case 3:
                camRect.x = 0.5f;
                camRect.y = 0.0f;
                break;
        }
        
        cam.rect = camRect;
    }

    public void OpenPauseMenu(InputAction.CallbackContext context)
    {
        if (gameStarted && context.performed)
        {
            Time.timeScale = 0f;
            Cursor.visible = true;
            pauseMenu.gameObject.SetActive(true);
        }
    }
}