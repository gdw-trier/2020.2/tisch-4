﻿using System.Collections.Generic;
using Systems;
using Systems.Skinning;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;
using Utilities;

public class PI_Manager : MonoSingleton<PI_Manager>
{
    public bool useGameMaster = false;
    public GameObject playerPrefab;
    public GameObject gmPrefab;
    [HideInInspector] public int splitScreens;
    [SerializeField] private bool useJoinLobby;

    [SerializeField] private TextMeshProUGUI pressStart;
    private PlayerInputManager _playerInputManager;
    public PlayerInputManager PlayerInputManager => _playerInputManager;

    private int _activeUsers = 0;
    private List<InputUser> joinedUsers;

    private int _activePlayers = 0;

    private GameMasterController gmController;
    private bool gmInitialized = false;

    private List<PlayerMovement> players;
    public List<PlayerMovement> Players => players;

    private Spawn_Helper _spawnHelper;
    [SerializeField] private PlayerJoinPanel _playerJoinPanel;
    private Skinner _Skinner;
    [SerializeField] private PauseMenu _pauseMenu;

    private void Start()
    {
        _playerInputManager = GetComponent<PlayerInputManager>();
        _spawnHelper = FindObjectOfType<Spawn_Helper>();
        _Skinner = GetComponent<Skinner>();

        if (useGameMaster)
            _playerInputManager.playerPrefab = gmPrefab;

        players = new List<PlayerMovement>();
        joinedUsers = new List<InputUser>();
    }

    public void OnPlayerJoined(PlayerInput playerInput)
    {
        joinedUsers.Add(playerInput.user);

        PlayerMovement playerMovement = playerInput.GetComponent<PlayerMovement>();
        if (playerMovement)
        {
            playerMovement.gameStarted = false;
            playerMovement.PlayerIndex = _activePlayers;

            if (_Skinner) _Skinner.Spawn(null, _activePlayers, at => playerMovement.gameObject);

            int Player1_Layer = LayerMask.NameToLayer("P1");
            playerMovement.RefocusCamera(Player1_Layer + _activeUsers, useJoinLobby);

            Vector3 spawnPos = _spawnHelper ? _spawnHelper.GetSpawnPos() : transform.position;

            playerInput.transform.position = spawnPos;
            playerInput.transform.rotation = Quaternion.Euler(0f, -90f, 0f);

            playerMovement.pauseMenu = _pauseMenu;

            playerInput.gameObject.name = "Player " + (++_activePlayers);

            players.Add(playerMovement);

            playerMovement.usesKBM = false;
            foreach (InputDevice inputDevice in playerInput.devices)
            {
                if (inputDevice.name.Contains("Keyboard"))
                {
                    playerMovement.usesKBM = true;
                    break;
                }
            }
        }

        if (!gmController)
            gmController = playerInput.GetComponent<GameMasterController>();

        if (gmController && !gmInitialized)
        {
            int Player1_Layer = LayerMask.NameToLayer("P1");
            gmController.RefocusCamera(Player1_Layer + _activeUsers, useJoinLobby);
            gmController.transform.position = Vector3.zero;
            gmController.transform.rotation = Quaternion.identity;
            gmController.usesKBM = !playerInput.currentControlScheme.Equals("Gamepad");
            gmInitialized = true;
            playerInput.gameObject.name = "GM";

            gmController.pauseMenu = _pauseMenu;
        }

        if (_playerJoinPanel)
            _playerJoinPanel.AddPlayer(playerInput.gameObject.name, playerInput.currentControlScheme, playerInput.user);

        _activeUsers++;

        if (_activeUsers >= 2)
            players[0].SplitscreenFix(_activeUsers);

        if (_activeUsers == 4)
        {
            int playerNr = 0;

            if (useGameMaster)
            {
                gmController.SplitscreenFixV2(playerNr++);
            }

            foreach (PlayerMovement player in players)
            {
                player.SplitscreenFixV2(playerNr++);
            }
        }


        if (pressStart)
            pressStart.enabled = false;

        if (useGameMaster)
        {
            _playerInputManager.playerPrefab = playerPrefab;

            if (gmController)
            {
                splitScreens = 0;
                switch (_activeUsers)
                {
                    case 0:
                    case 1:
                        splitScreens = 1;
                        break;
                    case 2:
                        splitScreens = 2;
                        break;
                    case 3:
                    case 4:
                        splitScreens = 4;
                        break;
                }

                if (_activeUsers > 1)
                    gmController.UpdateUIPosition(splitScreens);
            }
        }
    }

    public void OnGameStart()
    {
        foreach (PlayerMovement player in players)
        {
            player.OnGameStart();
        }

        if (gmController)
            gmController.OnGameStart();

        _playerInputManager.DisableJoining();
    }

    public void OnPlayerLeft(PlayerInput playerInput)
    {
        Debug.Log("Player left");

        _activeUsers--;
        _activePlayers--;

        if (pressStart && _activeUsers == 0)
            pressStart.enabled = true;

        if (_playerJoinPanel)
            _playerJoinPanel.RemovePlayer(playerInput.user);

        if (_activePlayers == 0)
        {
            FindObjectOfType<Podium>().Show(new List<int>());
        }
    }

    public void SetUseGameMaster(bool useGM)
    {
        useGameMaster = useGM;
        if (useGM)
            _playerInputManager.playerPrefab = gmPrefab;
        else
        {
            _playerInputManager.playerPrefab = playerPrefab;
        }
    }
}