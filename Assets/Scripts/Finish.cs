﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Finish : MonoBehaviour{
    public float secondsOfGlory = 3f;

    public int slowDown = 4;


    private void OnTriggerEnter(Collider other){
        if (other.gameObject.CompareTag("Player")){
            Debug.Log("Entered Goal: " + other.gameObject);

            var players = GameObject.FindGameObjectsWithTag("Player");
            Debug.Log("Players found: " + other.gameObject);

            foreach (var player in players){
                player.GetComponent<DisplayText>().SetText("verloren");
            }


            other.gameObject.GetComponent<DisplayText>().SetText("Gewonnen!");
            Debug.Log("Winner: " + other.gameObject);

            StartCoroutine(EndSession());
        }
    }

    private IEnumerator EndSession(){
        Debug.Log("EndSession");

        for (float i = 1.0f; i > 0.0f; i -= 1f / (1 << slowDown)){
            Time.timeScale = i;
            yield return null;
        }

        Time.timeScale = 0;

        yield return new WaitForSecondsRealtime(secondsOfGlory);

        SceneManager.LoadScene("MainScene");
    }
}